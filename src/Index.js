import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import App from './components/App';
import PoweredBy from './components/Powered-by';
import About from './components/About';
import Testimonials from './components/Testimonials';
import feedback from './components/feedback';



window.React = React;

render(
  (<Router history={hashHistory}>
    <Route path="/" component={App}>
      <Route path="/about" component={About} />
      <Route path="/poweredby" component={PoweredBy} />
      <Route path="/Testimonials" component={Testimonials} />
       <Route path="/feedback" component={feedback} />

    </Route>
  </Router>), document.getElementById('content')
);
