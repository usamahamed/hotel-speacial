import React from 'react';


const feedback = () => {
  

  return (
<div className="feedback">
  <h1>Feedback</h1>
  <p>What would you like to share with us?</p>
  <ul className="type">
    <li className="active"><a href="#"><i className="fa fa-bug"></i> FAQ</a></li>
    <li><a href="#"><i className="fa fa-question-circle"></i> Hotel Reviwes</a></li>
    <li><a href="#"><i className="fa fa-lightbulb-o"></i>Website Feedback </a></li>
  </ul>
  <div className="form">
    <textarea name="feedback_text" placeholder="Your feedback"></textarea>
    <input type="text" placeholder="Your Email (will not be published)" />
  </div>
  <p>How would you rate the speed of our website?</p>
  <ul className="type speed">
    <li><a href="#"><i className="fa fa-bolt"></i> Fast</a></li>
    <li><a href="#"><i className="fa fa-thumbs-o-up"></i> Fine</a></li>
    <li><a href="#"><i className="fa fa-thumbs-o-down"></i> Slow</a></li>
  </ul>
  <button className="button small success radius">Submit</button>
</div>

  );
};

export default feedback;
