import $ from 'jquary';

var $box = $('.box');

  $('.closei').each(function(){
    var color = $(this).css('backgroundColor');
    var content = $(this).html();
    $(this).click(function() {
    $box.css('backgroundColor', color);
    $box.addClass('open');
    $box.find('p').html(content);
  });
    
  $('.close').click(function() {
    $box.removeClass('open');
    $box.css('backgroundColor', 'transparent');
    
 });
  
  $('body').toggleClass('overlay');
$("#pop-toggle").click(function(){
  $(".popup").toggle();
  $('body').toggleClass('overlay');
})
$(".close").click(function(){
  $(".popup").toggle();
  $('body').toggleClass('overlay');
});
  
});


  $( "#enter-id" ).slideUp();


//Single date range pickers
$(function() {
    $('.js-single-daterange').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
});


$( "#submit-id-button" ).click(function() {
  $( this ).hide();
  /*$( "#submit-id" ).slideUp( "fast", function() {
    // Animation complete.
  });*/
  $( "#enter-id" ).slideDown( "fast", function() {
    // Animation complete.
  });
  $("#enter-id-button").removeAttr('disabled');
  
});




$('.next').click(function(){
    var nextId = $(this).parents('.tab-pane').next().attr("id");
    $('[href=#'+nextId+']').tab('show');
    return false;
  })

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    //update progress
    var step = $(e.target).data('step');
    var percent = (parseInt(step) / 4) * 100;

    $('.progress-bar').css({width: percent + '%'});
    //$('.progress-bar').text("Step " + step + " of 5");
    e.relatedTarget // previous tab
  })

  $('.first').click(function(){
    $('#myWizard a:first').tab('show')
  })
  
  
  $('#last').click(function(){
    $('#lastli').addClass('success');
  });

  $('.owl-carousel.testimonial-carousel').owlCarousel({
    nav: true,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    dots: false,
    responsive: {
      0: {
        items: 1,
      },
      750: {
        items: 2,
      }
    }
  });
  
