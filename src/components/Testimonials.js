import React from 'react';


const Testimonials = () => {
  

  return (
<div className="owl-carousel testimonial-carousel">

  <div className="single-testimonial">
    <div className="testimonials-wrapper">
      <h4>Habitasse lobortis cum malesuada nullam cras odio venenatis nisl at turpis sem in porta consequat massa a mus massa nascetur elit vestibulum a.</h4>
      <div className="testimonials-blob"></div>
      <div className="testimonials-img"><img src="https://randomuser.me/api/portraits/women/21.jpg"/></div>
      <div className="testimonials-person-info">
        <p><b>Jane Doe</b><br />Web Developer</p>
      </div>
    </div>
  </div>
  <div className="single-testimonial">
    <div className="testimonials-wrapper">
      <h4>Aenean a neque ipsum. In viverra mauris nibh, nec dapibus nibh imperdiet at. Nulla urna odio, aliquam tincidunt posuere quis, placerat nec sem.</h4>
      <div className="testimonials-blob"></div>
      <div className="testimonials-img"><img src="https://randomuser.me/api/portraits/women/65.jpg"/></div>
      <div className="testimonials-person-info">
        <p><b>Jane Doe</b><br />Web Developer</p>
      </div>
    </div>
  </div>
 
</div>
  );
};

export default Testimonials;
