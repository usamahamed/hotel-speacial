import React from 'react';
import { Link } from 'react-router';
import { version } from '../../package.json';

const App = ({ children }) => (
  <div>
    <header>
      <Link to="/"><h1>Hotel Speacial</h1></Link>
      <Link to="/about">Form Validation</Link>
      <Link to="/poweredby">Subscribe</Link>
      <Link to="/Testimonials">Testimonials</Link>
      <Link to="/feedback">Feedback </Link>


    </header>
    <section>
      {children || <div class='popScroll'> <h1>Explanation of feature</h1>
      <p>1- in the main form searching for hotel the form need some validation for example it must not propegate form with empty fields such as hotel name input</p>
      <p>2- every user must enter site must find functionality that can Subscribe to site to recive the last offers and leatest price and more</p>
      <p>3- it also can write testimonials for users and what they say about service</p>
      <p>4- it also can write feedback or report about for example some hotel or some problem in site</p>
      <p>5- it will be good for site to be interactive with users to have some online chatting service that can reply to him soon</p>
      <h1>suggestion</h1>
      <p>1- the meta tag in frontend source code missing some main keywords that not good for SEO</p>
      <p>2- The site contain seven scripts that can bundel together for proformance and speed</p>
      <p>3- The site can access through User-agent Libwww-per that are used by many script kiddy crackers and spam bots</p>

      </div>}
    </section>
  </div>
);

App.propTypes = { children: React.PropTypes.object };

export default App;
